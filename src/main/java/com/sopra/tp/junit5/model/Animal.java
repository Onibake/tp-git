package com.sopra.tp.junit5.model;

public class Animal {

	private String type;
	
	private String nom;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String toString()
	{
		return "(type:" + type + "|nom:" + nom + ")";
	}

}
