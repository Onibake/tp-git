package com.sopra.tp.junit5.model;

import java.util.List;
import java.util.function.Supplier;

public class Personne {

	private String prenom;
	
	private String nom;
	
	private List<Animal> animaux;
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Animal> getAnimaux() {
		return animaux;
	}

	public void setAnimaux(List<Animal> animaux) {
		this.animaux = animaux;
	}

	public String toString()
	{
		return "(pr�nom:" + prenom + "|nom:" + nom + "|animaux:" + (animaux != null ? animaux.toString() : "aucun") + ")";
	}
	
	public Supplier<String> getToStringFunction()
	{
		return this::toString;
	}

}
