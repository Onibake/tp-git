package com.sopra.tp.junit5.function;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import com.sopra.tp.junit5.model.Animal;
import com.sopra.tp.junit5.model.Personne;

public class GetFavoriteAnimalType implements Function<Personne, String> {

	private Function<List<Animal>, Map<String, List<Animal>>> groupByFunction;
	
	public GetFavoriteAnimalType(Function<List<Animal>, Map<String, List<Animal>>> groupByFunction) {
		
		// Precondictions.checkNotNull(groupByFunction, "La m�thode GroupBy ne peut pas �tre null");		
		if(groupByFunction == null) {
			throw new NullPointerException("La m�thode GroupBy ne peut pas �tre null");
		}
		
		this.groupByFunction = groupByFunction;
	}
	
	@Override
	public String apply(Personne personne) {
 
		if (personne == null) {
			throw new NullPointerException("La personne ne peut pas �tre null");
		}

		Map<String, List<Animal>> mapTypes = Optional.of(personne)
				.map(Personne::getAnimaux)
				.map(this.groupByFunction)
				//.map(new GroupAnimalsByType())				
				// �gale � .map(animaux -> new GroupAnimalsByType().apply(animaux))
				.orElse(Collections.emptyMap());
								
		return mapTypes.entrySet().stream()
			.max((entry1, entry2) -> entry1.getValue().size() - entry2.getValue().size())
			.map(Entry::getKey)
			.orElse("Pas de type favori");			
	}

}
