package com.sopra.tp.junit5.function;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.sopra.tp.junit5.model.Animal;

public class GroupAnimalsByType implements Function<List<Animal>, Map<String, List<Animal>>> {

	@Override
	public Map<String, List<Animal>> apply(List<Animal> animaux) {
		return animaux.stream()			
				.filter(Objects::nonNull)
				.collect(Collectors.groupingBy(Animal::getType));			
	}

}
