package com.sopra.tp.junit5;


import java.util.Arrays;

import com.sopra.tp.junit5.function.GetFavoriteAnimalType;
import com.sopra.tp.junit5.function.GroupAnimalsByType;
import com.sopra.tp.junit5.model.Animal;
import com.sopra.tp.junit5.model.Personne;

public class Main {
	
	public static void main(String[] args) {				
	
		Animal chat1 = new Animal();
		chat1.setNom("chat1");
		chat1.setType("chat");
		
		Animal chat2 = new Animal();
		chat2.setNom("chat2");
		chat2.setType("chat");				
		
		Animal chien1 = new Animal();
		chien1.setNom("chien1");
		chien1.setType("chien");
		
		Animal poisson1 = new Animal();
		poisson1.setNom("poisson1");
		poisson1.setType("poisson");
		
		Animal poisson2 = new Animal();
		poisson2.setNom("poisson2");
		poisson2.setType("poisson");
		
		Animal poisson3 = new Animal();
		poisson3.setNom("poisson3");
		poisson3.setType("poisson");
		
		Personne personne1 = new Personne();
		personne1.setPrenom("prenomPersonne1");
		personne1.setNom("nomPersonne1");
		personne1.setAnimaux(Arrays.asList(chat1, chat2, chien1, poisson1, poisson2, poisson3));
		
		String type = new GetFavoriteAnimalType(new GroupAnimalsByType()).apply(personne1);
		
		System.out.println(type);
	}

}
