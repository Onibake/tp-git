package com.sopra.tp.junit5.function;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.sopra.tp.junit5.model.Animal;
import com.sopra.tp.junit5.model.Personne;

/**
 * Tests unitaires de la classe {@link GetFavoriteAnimalType}.
 * 
 * @author jsola
 */
@TestInstance(Lifecycle.PER_CLASS)
public class GetFavoriteAnimalTypeTest {
		
	@Mock
	private GroupAnimalsByType groupAnimalsByType;
	
	private AutoCloseable closeable;
	
	private GetFavoriteAnimalType getFavoriteAnimalType;
	
	private Personne personne;
	
	//private int count;
		
	/**
	 * Ouvre les mocks de la classe.
	 */
    @BeforeAll 
    public void beforeAll() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    /**
     * Ferme les mocks de la classe.
     * 
     * @throws Exception exception non attendue
     */
    @AfterAll 
    public void afterAll() throws Exception {
        closeable.close();
    }
	
	/**
	 * Initialise les mocks et les variables avant chaque test.
	 */
	@BeforeEach
	public void beforeEach() {
		
		Animal chat1 = new Animal();
		chat1.setNom("chat1");
		chat1.setType("chat");
		
		Animal chat2 = new Animal();
		chat2.setNom("chat2");
		chat2.setType("chat");				
		
		Animal chien1 = new Animal();
		chien1.setNom("chien1");
		chien1.setType("chien");
		
		Animal poisson1 = new Animal();
		poisson1.setNom("poisson1");
		poisson1.setType("poisson");
		
		Animal poisson2 = new Animal();
		poisson2.setNom("poisson2");
		poisson2.setType("poisson");
		
		Animal poisson3 = new Animal();
		poisson3.setNom("poisson3");
		poisson3.setType("poisson");
		
		this.personne = new Personne();
		this.personne.setPrenom("prenomPersonne1");
		this.personne.setNom("nomPersonne1");
		this.personne.setAnimaux(Arrays.asList(chat1, chat2, chien1, poisson1, poisson2, poisson3));
		
		// Chez ASSU nous utilisons initMocks au lieu de openMocks
		//MockitoAnnotations.initMocks(this);
		
		this.getFavoriteAnimalType = new GetFavoriteAnimalType(new GroupAnimalsByType());		
	}
	
	/**
	 * Reset les mocks.
	 */
	@AfterEach
	public void afterEach() {
		Mockito.reset(this.groupAnimalsByType);
	}	
	
	/**
	 * Test du constructeur lorsque la fonction de GroupBy est null.<br>
	 * Le retour attendu est une {@link NullPointerException}.
	 */
	@Test
	public void constructeur_IfGroupByNull_ThenThrowNullPointerException() {
		// When		
		Exception exception = Assertions.assertThrows(NullPointerException.class, () -> new GetFavoriteAnimalType(null));
		
		// Then
		Assertions.assertEquals("La m�thode GroupBy ne peut pas �tre null", exception.getMessage());
	}
	
	/**
	 * Test de la m�thode apply lorsque la personne est null.<br>
	 * Le retour attendu est une {@link NullPointerException}.
	 */
	@Test
	@DisplayName("test display")
	public void apply_IfPersonneNull_ThenThrowNullPointerException() {
		// When		
		Exception exception = Assertions.assertThrows(NullPointerException.class, () -> this.getFavoriteAnimalType.apply(null));
		
		// Then
		// *** Exemple avec message et supplier ***
		//Assertions.assertNotEquals("La personne ne peut pas �tre null", exception.getMessage(), "test message");
		//Assertions.assertNotEquals("La personne ne peut pas �tre null", exception.getMessage(), () -> "test message 2");
		
		// *** Exemple assertAll ***
		// Assertions.assertAll("Message assertAll", 
		//	    () -> Assertions.assertNull(exception, "L'exception doit �tre null"),
		//	    () -> Assertions.assertNull(exception.getMessage(), "Le message doit �tre null")
		//	);		
		//	Assertions.assertNull(exception, "L'exception doit �tre null");
		//	Assertions.assertNull(exception.getMessage(), "Le message doit �tre null");
		
		Assertions.assertEquals("La personne ne peut pas �tre null", exception.getMessage());
	}
	
	/**
	 * Test de la m�thode apply lorsque qu'il y a un type d'animal favori.<br>
	 * Le retour attendu est le type favori.
	 */	
	@RepeatedTest(value = 3)
	public void apply_IfHas1TypeFavori_ThenTypeFavori(RepetitionInfo info) {
		
		System.out.println("Test " + info.getCurrentRepetition() + "/" + info.getTotalRepetitions());
		
		// When		
		String typeFavori = this.getFavoriteAnimalType.apply(this.personne);
		
		// Then
		Assertions.assertEquals("poisson", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque qu'il y a un type d'animal favori.<br>
	 * Le retour attendu est le type favori.
	 */
	@ParameterizedTest
	@MethodSource("getPersonnes")	
	public void apply_IfHas1TypeFavori_ThenTypeFavori_ParameterizedTest(Entry<Personne, String> entry) {		
		// When		
		String typeFavori = this.getFavoriteAnimalType.apply(entry.getKey());
		
		// Then
		Assertions.assertEquals(entry.getValue(), typeFavori);
	}
		  	 
	static Stream<Entry<Personne, String>> getPersonnes() {
		
		Animal chat1 = new Animal();
		chat1.setNom("chat1");
		chat1.setType("chat");					
		
		Animal chien1 = new Animal();
		chien1.setNom("chien1");
		chien1.setType("chien");
		
		Animal poisson1 = new Animal();
		poisson1.setNom("poisson1");
		poisson1.setType("poisson");
		
		Personne personne1 = new Personne();
		personne1.setAnimaux(Collections.singletonList(chat1));
		
		Personne personne2 = new Personne();
		personne2.setAnimaux(Collections.singletonList(chien1));
		
		Personne personne3 = new Personne();
		personne3.setAnimaux(Collections.singletonList(poisson1));
				
		Map<Personne, String> map = new HashMap<>();
		map.put(personne1, "chat");
		map.put(personne2, "chien");
		map.put(personne3, "poisson");
				
		return map.entrySet().stream();
	}
	
	/**
	 * Test de la m�thode apply lorsque qu'il y a un type d'animal favori.<br>
	 * Le retour attendu est le type favori.
	 */
	@TestFactory	
	public List<DynamicTest> apply_IfHas1TypeFavori_ThenTypeFavori_TestFactory() {					
		   return getPersonnes()
				   .map(entry -> DynamicTest.dynamicTest("Test -> " + entry.getValue(), () -> {
			   			// When		
						String typeFavori = this.getFavoriteAnimalType.apply(entry.getKey());
						
						// Then
						Assertions.assertEquals(entry.getValue(), typeFavori);
				   })).collect(Collectors.toList());						
	}
	
	/**
	 * Test de la m�thode apply lorsque qu'il y a deux types d'animaux favoris.<br>
	 * Le retour attendu est le premier type rencontr�.
	 */
	@Test
	public void apply_IfHas2TypesFavoris_ThenFirstTypeFavori() {
		// Given
		Animal chat2 = new Animal();
		chat2.setNom("chat2");
		chat2.setType("chat");				
		
		Animal chien1 = new Animal();
		chien1.setNom("chien1");
		chien1.setType("chien");
		
		this.personne.setAnimaux(Arrays.asList(chien1, chat2));		
		
		// When		
		String typeFavori = this.getFavoriteAnimalType.apply(this.personne);
		
		// Then
		Assertions.assertEquals("chat", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque la liste des animaux est vide.<br>
	 * Le retour attendu est pas de type favori.
	 */
	@Test
	public void apply_IfAnimauxEmpty_ThenNoTypeFavori() {
		// Given
		this.personne.setAnimaux(Collections.emptyList());
		
		// When		
		String typeFavori = this.getFavoriteAnimalType.apply(this.personne);
		
		// Then
		Assertions.assertEquals("Pas de type favori", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque la liste des animaux est null.<br>
	 * Le retour attendu est pas de type favori.
	 */
	@Test
	public void apply_IfAnimauxNull_ThenNoTypeFavori() {
		// Given
		this.personne.setAnimaux(null);
		
		// When		
		String typeFavori = this.getFavoriteAnimalType.apply(this.personne);
		
		// Then
		Assertions.assertEquals("Pas de type favori", typeFavori);
	}
	
	
	/**
	 * Test de la m�thode apply lorsque la liste des animaux contient des �l�ments null.<br>
	 * Le retour attendu est pas de type favori.
	 */
	@Test
	public void apply_IfAnimauxWithNull_ThenNoTypeFavori() {
		// Given
		this.personne.setAnimaux(Arrays.asList(null, null));
		
		// When		
		String typeFavori = this.getFavoriteAnimalType.apply(this.personne);
		
		// Then
		Assertions.assertEquals("Pas de type favori", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque la fonction de GroupBy retourne null.<br>
	 * Le retour attendu est pas de type favori.
	 */
	@Test
	public void apply_IfGroupByReturnNull_ThenNoTypeFavori() {
		// Given
		Mockito.when(this.groupAnimalsByType.apply(ArgumentMatchers.anyList())).thenReturn(null);
		
		// When		
		String typeFavori = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);		
		
		// Then
		Assertions.assertEquals("Pas de type favori", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque la fonction de GroupBy retourne une liste vide.<br>
	 * Le retour attendu est pas de type favori.
	 */
	@Test
	public void apply_IfGroupByReturnEmptyMap_ThenNoTypeFavori() {
		
		// Test bonus Mockito
		
		// Given
		Mockito.when(this.groupAnimalsByType.apply(ArgumentMatchers.anyList())).thenReturn(Collections.emptyMap());
		
		// When		
		String typeFavori = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);		
		
		// Then
		Assertions.assertEquals("Pas de type favori", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque la fonction de GroupBy lance une {@link NullPointerException}.<br>
	 * Le retour attendu est une {@link NullPointerException}.
	 */
	@Test
	public void apply_IfGroupByThrowNullPointerException_ThenThrowNullPointerException() {
		
		// Test bonus Mockito
		
		// Given
		Mockito.when(this.groupAnimalsByType.apply(ArgumentMatchers.anyList())).thenThrow(new NullPointerException("Test mockito"));
		
		// When		
		Exception exception = Assertions.assertThrows(NullPointerException.class, () -> new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne));
		
		// Then
		Assertions.assertEquals("Test mockito", exception.getMessage());	
	}
	
	/**
	 * Test de la m�thode apply lorsque la fonction de GroupBy retourne une map non vide.<br>
	 * Le retour attendu est un type favori.
	 */
	@Test
	public void apply_IfGroupByReturnNotEmptyMap_ThenTypeFavori() {
		
		// Test bonus Mockito
		
		// Given
		Mockito.when(this.groupAnimalsByType.apply(ArgumentMatchers.anyList())).thenReturn(Collections.singletonMap("dragon", Collections.singletonList(new Animal())));
		
		// When		
		String typeFavori = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);		
		
		// Then
		Assertions.assertEquals("dragon", typeFavori);
	}
	
	/**
	 * Test de la m�thode apply lorsque la fonction de GroupBy est appel�e 2 fois.<br>
	 * Le retour attendu est 2 types favoris diff�rents.
	 */
	@Test
	public void apply_IfGroupByCall2Times_Then2TypesFavoris() {
		
		// Test bonus Mockito
		
		// Given		
		//this.count = 0; 		
		Mockito.when(this.groupAnimalsByType.apply(ArgumentMatchers.anyList())).thenAnswer(
				new Answer<Map<String, List<Animal>>>() {
					@Override
					public Map<String, List<Animal>> answer(InvocationOnMock invocation) throws Throwable {																		
						// count++;																	
						if(Mockito.mockingDetails(groupAnimalsByType).getInvocations().size() == 1) {
							return Collections.singletonMap("dragon", Collections.singletonList(new Animal()));
						}						
						return Collections.singletonMap("poussin", Collections.singletonList(new Animal()));
					}
				}
		);
		
		// Hola
		
		// When		
		String typeFavori1 = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);		
		String typeFavori2 = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);
		
		// Then		
		Mockito.verify(this.groupAnimalsByType, Mockito.times(2)).apply(ArgumentMatchers.anyList());		
		Assertions.assertEquals("dragon", typeFavori1);
		Assertions.assertEquals("poussin", typeFavori2);
	}
	
	/**
	 * Test de la m�thode apply lorsque la fonction de GroupBy est appel�e 2 fois.<br>
	 * Le retour attendu est 2 types favoris diff�rents.
	 */
	@Test
	public void apply_IfGroupByCall2Times_Then2TypesFavoris_LAMBDA() {
		
		// Test bonus Mockito
		
		Answer<Map<String, List<Animal>>> answer = invocation -> {
			if(Mockito.mockingDetails(groupAnimalsByType).getInvocations().size() == 1) {
				return Collections.singletonMap("dragon", Collections.singletonList(new Animal()));
			}						
			return Collections.singletonMap("poussin", Collections.singletonList(new Animal()));
		};						
		
		// Given		 		
		Mockito.when(this.groupAnimalsByType.apply(ArgumentMatchers.anyList())).thenAnswer(answer);
		
		// When		
		String typeFavori1 = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);		
		String typeFavori2 = new GetFavoriteAnimalType(this.groupAnimalsByType).apply(this.personne);
		
		// Then
		Assertions.assertEquals("dragon", typeFavori1);
		Assertions.assertEquals("poussin", typeFavori2);
	}

}
